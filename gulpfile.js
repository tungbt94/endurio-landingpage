var gulp = require('gulp');
var	watch = require('gulp-watch');
var	sass = require('gulp-sass');
var	browserSync = require('browser-sync').create();

// Static Server + watching scss/html files
gulp.task('serve', function() {
  browserSync.init({
      server: "./html"
  });
  gulp.watch("scss/*.scss", ['sass']);
  gulp.watch("html/index.html").on('change', browserSync.reload);
});

gulp.task('sass', function () {
	gulp.src("scss/*.scss")
		.pipe(sass({outputStyle: 'expanded'}))
		.pipe(gulp.dest('html/assets/css'))
		.pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('default', ['serve']);